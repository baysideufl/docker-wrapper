# Docker Compose Skeleton!

## Getting Started

- Update the `docker/scripts/.functions.sh` file. Update `projecturl` and `port` with your project specifics.
- Set the project template (docker setup) with `bin/template <template name>`.
- Update the `docker/scripts/setup.sh` file. Include any project-specific set up scripts.
- Run the project setup: `bin/setup`.
- Bring up the project: `bin/up`.

**NOTES:**
- The `docker/conf` dir is just a symlink to the template directory. Do not modify the symlink directly.
- The docker-compose.yml will be copied from the template directory when using the `bin/template` script. Do no modify `docker-compose.yml` directly.


## Commands

```bash
# from project root:
$> bin/setup # checks your host file, checks the loopback alias and any other tasks
$> bin/up # brings docker containers up
$> bin/down # shuts docker containers down

# set project template
$> bin/template <template name> # symlinks docker/conf -> docker/templates/<template name>
$> bin/template php-apache #example
```

## Configuration

The following files should be found in the `docker/templates/<template name>` dir for each template.

- `docker-compose.yml`
  - specify exposed ports (left side of colon). Note: these must be unique across your host computer
  - in the wordpress service, specify the location where you want your project files to be synced
    - `./app:/var/www/html` means sync the files in the `app` dir to the container and put them in the `/var/www/html` dir.
    - if you modify this location you must also update the `conf/virtualhost.conf` to match, otherwise Apache won't be able to find the site files
    - you must also update the `pathMappings` in the `.vscode/launch.json` file if you want debugging to work
- `apache2.conf`
  - server config. you probably don't need to mess with this too much
- `virtualhost.conf`
  - this is for your apache virtualhost. it's very generic and will accept connections on any host name.
  - make sure the paths in this file match the volume you mounted in the `docker-compose.yml` file.
- `<template-name>.dockerfile`
  - installs: wordpress, xdebug and then restarts apache
  - any other provisioning items can go here (ie: apt packages, etc)
- `xdebug.ini`
  - configuration for xdebug. make sure the port matches what's in your `.vscode/launch.json`.
  - changing any values requires a docker restart
  - this require a loopback alias on your host to be set up at `10.254.1.1`. Run `docker/setup` to add this interface on mac.

## Details

- `app`
  - your application code goes here
- `docker/.data`
  - this is where the container syncs your database files. Should be gitignored, don't mess with this dir. It's there so that your database remains if you need to rebuild your container.
- `docker/logs`
  - logs from the container will show up here (configure the synced logs in the `docker-compose.yml`)
