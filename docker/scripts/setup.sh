#!/usr/bin/env bash

source docker/scripts/.functions.sh

BLUE='\033[0;34m'
GREEN='\033[0;32m'
NC='\033[0m'

checkHost
checkAlias

####### Add your setup routines here ##########

# cd app
# composer install
# npm install
# gulp compile
# cd ..

####### Don't modify below this line ###########


echo -e ""
echo -e "\033[15;48;5;69m                            All Done!                                     \033[0;49;39m"

