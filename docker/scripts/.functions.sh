#!/bin/bash

BLUE='\033[0;34m'
GREEN='\033[0;32m'
RED='\033[0;31m'
NC='\033[0m'

projecturl="mysite.local"
port="8888"

function checkHost() {
  echo -e "${BLUE}Checking Hosts file${NC}"

  if grep -Rq " $projecturl" "/etc/hosts"
  then
      echo -e "${BLUE}Hosts file is up to date.${NC}"
  else
      sudo sh -c "echo 127.0.0.1 "$projecturl" >> /etc/hosts"
  fi

}

function checkAlias() {
  echo -e "${BLUE}Checking Loopback Alias${NC}"

  loopback="$(ifconfig lo0 | grep ' 10.254.1.1')"
  if  [ "$loopback"1 == "1" ]; then
    echo -e "\033[91mNote\033[39m: We noticed your loopback is gone we are fixing this now."
    echo "Please enter your password when asked."
    sudo ifconfig lo0 alias 10.254.1.1
  fi
}

function printDone() {
  echo -e "${BLUE}Accelerating to 88mph${NC}"
  echo -e ""
  echo -e "\033[15;48;5;69m                            All Done. Enjoy!                                     \033[0;49;39m"
  echo -e ""
}
