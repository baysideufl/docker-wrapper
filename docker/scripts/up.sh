#!/usr/bin/env bash

source docker/scripts/.functions.sh

BLUE='\033[0;34m'
GREEN='\033[0;32m'
NC='\033[0m'

checkAlias

echo -e "${BLUE}Starting environment${NC}"
docker-compose up -d

echo -e ""
echo -e "\033[15;48;5;69m                            All Done. Enjoy!                                     \033[0;49;39m"
echo -e ""
echo -e "${GREEN}http://localhost:${port}"
echo -e "${GREEN}http://${projecturl}:${port}"
echo -e ""

