#!/usr/bin/env bash

source docker/scripts/.functions.sh

echo -e "${BLUE}Setting project template${NC}"

cd docker
SOURCE="templates/$1"
TARGET="conf"

if [ $1 ] && [ -d "$SOURCE" ]; then
  # remove the old directory / symlink
  if [ -d "$TARGET" ]; then
    if [ -L "$TARGET" ]; then # symlink
      echo -e "${BLUE}Removing previous symlink${NC}"
      rm "$TARGET"
    else # dir
      echo -e "${BLUE}Removing previous directory${NC}"
      rmdir "$TARGET"
    fi
  fi

  echo -e "${BLUE}Linking template${NC}"
  ln -s $SOURCE $TARGET
  echo -e "${BLUE}Copying docker-compose.yml${NC}"
  cp "$SOURCE/docker-compose.yml" ../docker-compose.yml
  printDone
  cd ..
else
  echo -e "${RED}Please enter the name of a valid project:${BLUE}"
  cd templates
  ls -d */ | cut -f1 -d'/'
fi
