#!/usr/bin/env bash

BLUE='\033[0;34m'
GREEN='\033[0;32m'
NC='\033[0m'


echo -e "${BLUE}Shuttin' 'er down${NC}"
docker-compose down

echo -e "${GREEN}DONE!${NC}"
